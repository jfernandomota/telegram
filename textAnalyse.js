const natural = require('natural')
const removeAccents = require('remove-accents')

const actions = [
    {
        action: '1',
        verbs: ['ligar', 'acender'],
        pronouns: ['lampada', 'luz', 'claridade']
    },
    {
        action: '2',
        verbs: ['apagar', 'desligar'],
        pronouns: ['lampada', 'luz', 'claridade']
    },
    {
        action: '3',
        verbs: ['ligar', 'acender'],
        pronouns: ['vento', 'fan', 'fa', 'ventilador', 'cooler']
    },
    {
        action: '4',
        verbs: ['apagar', 'desligar'],
        pronouns: ['vento', 'fan', 'fa', 'ventilador', 'cooler']
    }
]

const clearString = text => 
    removeAccents(text)
        .toLowerCase()

const analyse = text => {
    natural.PorterStemmerPt.attach()
    const textTokenized = clearString(text).tokenizeAndStem()

    return actions.find(action => {
        const verbs = action.verbs.map(verb => clearString(verb).stem())
        const pronouns = action.pronouns.map(pron => clearString(pron).stem())

        return verbs.find(verb => textTokenized.includes(verb)) && pronouns.find(pron => textTokenized.includes(pron))
    })
}

module.exports = {analyse}