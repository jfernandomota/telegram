process.env.NTBA_FIX_319 = "DISABLED"
process.env.GOOGLE_APPLICATION_CREDENTIALS = "./config/gcloud.json"

const restify = require('restify')
const socketIO = require('socket.io')
const fs = require('fs')
const fetch = require('node-fetch')
const speech = require('@google-cloud/speech')
const natural = require('natural')
const {analyse} = require('./textAnalyse')
const _ = require('lodash')

const Promise = require('bluebird')
Promise.config({ cancellation: true })

const TelegramBot = require('node-telegram-bot-api')
const mqtt = require('async-mqtt')

const server = restify.createServer()
const io = socketIO(server.server)
const connections = {}

const TOKEN = '739463327:AAFjiBlymo4xHVIHyl080miPFlhsYtP9VSM'
const MQTT_SERVER = "mqtt://0.0.0.0"
const MQTT_PORT = 1883
const TELEGRAM_FILES = `https://api.telegram.org/bot${TOKEN}/getFile`
const TELEGRAM_FILES_CONTENT = `https://api.telegram.org/file/bot${TOKEN}`
const PASSWORD = '123'

const bot = new TelegramBot(TOKEN, { polling: true })
const speechClient = new speech.SpeechClient()
const mqttClient = mqtt.connect(MQTT_SERVER, { port: MQTT_PORT })
const USERS = []

io.on('connection', socket => {
    connections[socket.id] = socket

    socket.on('action', ({ action }) => {
        if (action) {
            mqttClient.publish('telegram/action', action)
        }
    })

    socket.on('disconnect', () => delete connections[socket.id])
})

const throttle = (fun, time) => {
    let lastExecution = 0

    return (...args) => {
        const now = new Date().getTime()
        const nextExecution = lastExecution + time

        if (nextExecution <= now) {
            lastExecution = now
            fun(args)
        }
    }
}

const notifyAllSockets = (topic, buffer) => {
    Object
        .values(connections)
        .forEach(socket =>
            socket.emit(topic, buffer)
        )
}

async function init() {
    mqttClient.subscribe('telegram/movement')
    mqttClient.subscribe('telegram/smoke')
    mqttClient.subscribe('telegram/smokea')
    mqttClient.subscribe('telegram/humidity')
    mqttClient.subscribe('telegram/temperature')
    mqttClient.subscribe('telegram/light')
    mqttClient.subscribe('telegram/fan')
    mqttClient.subscribe('telegram/movsensor')

    bot.onText(/\/help/, msg => {
        const chatId = msg.chat.id

        bot.sendMessage(chatId, `
            /auth para autenticar\n
        `)
    })

    bot.onText(/\/auth (.+)/, (msg, match) => {
        const chatId = msg.chat.id

        if (match[1] === PASSWORD) {
            USERS.push(chatId)
            return bot.sendMessage(chatId, "OK, você está dentro!!!")
        }

        bot.sendMessage(chatId, "Senha inválida!")
    })

    const notifySmoke = throttle(() => {
        const audio = fs.createReadStream('./assets/audios/fire.mp3')
        const options = {
            title: "ATENÇÃO",
            caption: "Olha o que o Faustão disse sobre isso..."
        }

        const fileOptions = {
            filename: 'eita.mp3',
            contentType: 'application/octet-stream',
        }

        USERS.forEach(id => {
            bot.sendMessage(
                id,
                `<b>ATENÇÃO!</b>\nFoi detectado fumaça ou gás em sua residência!!!`,
                { parse_mode: 'HTML' }
            )
            bot.sendAudio(id, audio, options, fileOptions)
        })
    }, 120000)

    const notifyMovement = throttle(() => {
        USERS.forEach(id => {
            bot.sendMessage(
                id,
                `<b>ATENÇÃO!</b>\nFoi detectado movimento em sua residência!!!`,
                { parse_mode: 'HTML' }
            )
        })
    }, 120000)

    const extractText = ({ text, voice }) => {
        if (text) {
            return Promise.resolve(text)
        }

        return fetch(`${TELEGRAM_FILES}?file_id=${voice.file_id}`)
            .then(resp => resp.json())
            .then(({ ok, result }) => ok ? result : Promise.reject(result))
            .then(({ file_path }) => fetch(`${TELEGRAM_FILES_CONTENT}/${file_path}`))
            .then(resp => resp.arrayBuffer())
            .then(buffer => {
                const content = new Buffer(buffer).toString('base64')

                return speechClient
                    .recognize({
                        config: {
                            encoding: 'OGG_OPUS',
                            sampleRateHertz: 16000,
                            languageCode: 'pt-BR'
                        },
                        audio: { content }
                    })
                    .then(([{ results }]) =>
                        _.flatMap(results, ({ alternatives }) => alternatives)
                            .map(({ transcript }) => transcript)
                            .join(" ")
                    )
            })
            .catch(() => '')
    }

    bot.on('message', message => {
        extractText(message)
            .then(text => analyse(text))
            .then(action => {
                if (action) {
                    console.log(action)
                    mqttClient.publish('telegram/action', action.action)
                }
            })
    })

    mqttClient.on('message', (topic, buffer) => {
        try {
            buffer = JSON.parse(buffer.toString())

            switch (topic) {
                case 'telegram/movement':
                    if (buffer)
                        notifyMovement(topic, buffer)
                    break
                case 'telegram/smoke':
                    if (buffer)
                        notifySmoke(topic, buffer)
                    break
            }

            notifyAllSockets(topic, buffer)
        } catch (_) { }
    })
}

mqttClient.on('connect', init)

server.listen(3000, '0.0.0.0', () => console.log("Server started"))