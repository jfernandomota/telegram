import io from 'socket.io-client'

const checkButtonActive = (button, isActive, className = 'active') => {
    if (isActive) {
        if (!button.classList.contains(className))
            button.classList.add(className)
    } else if (button.classList.contains(className)) {
        button.classList.remove(className)
    }
}

const debounce = (fun, time) => {
    let timeout = 0

    return () => {
        clearTimeout(timeout)
        timeout = setTimeout(fun, time)
    }
}

const throttle = (fun, time) => {
    let lastExecution = 0

    return (...args) => {
        const now = new Date().getTime()
        const nextExecution = lastExecution + time

        if (nextExecution <= now) {
            lastExecution = now
            fun(args)
        }
    }
}

const throttlePromise = promise => {
    let isActive = false

    return () => {
        if (!isActive) {
            isActive = true
            promise().finally(() => isActive = false)
        }
    }
}

let state = {
    temperature: 0,
    humidity: 0,
    smokea: 0,
    smoke: false,
    movement: false,
    movsensor: true,
    light: false,
    fan: false
}

const renderScreen = debounce(() => {
    const { temperature, humidity, light, fan, movement, movsensor, smoke } = state
    document.querySelector('#temp').textContent = parseInt(temperature)
    document.querySelector('#humi').textContent = parseInt(humidity)

    checkButtonActive(document.querySelector('#lamp'), light)
    checkButtonActive(document.querySelector('#fan'), fan)
    checkButtonActive(document.querySelector('#move'), movsensor, 'active')
    checkButtonActive(document.querySelector('#move'), movement, 'alert')
    checkButtonActive(document.querySelector('#smoke'), smoke, 'alert')
}, 1000)

const faustaoAudio = throttle(() => {
    const audio = new Audio('/assets/audios/fire.mp3')
    return audio.play()
}, 120000)

const setState = (newState) => {
    state = newState
    renderScreen()
}

window.onload = () => {
    const socket = io('http://localhost:3000')

    window.sendAction = action => {
        switch (action) {
            case '1':
                socket.emit('action', { action: state.light ? '2' : '1' })
                break
            case '2':
                socket.emit('action', { action: state.fan ? '4' : '3' })
                break
            case '7':
                socket.emit('action', { action: state.movement ? '8' : '7' })
                setState({ ...state, movement: 0 })
                break
        }
    }

    socket.on('telegram/movement', movement =>
        setState({
            ...state,
            movement: state.movement || movement
        })
    )

    socket.on('telegram/smoke', smoke => {
        if (smoke) faustaoAudio()
        setState({ ...state, smoke })
    })

    socket.on('telegram/smokea', smokea => setState({ ...state, smokea }))

    socket.on('telegram/humidity', humidity => setState({ ...state, humidity }))

    socket.on('telegram/temperature', temperature => setState({ ...state, temperature }))

    socket.on('telegram/light', light => setState({ ...state, light }))

    socket.on('telegram/fan', fan => setState({ ...state, fan }))

    socket.on('telegram/movsensor', movsensor => setState({ ...state, movsensor }))
}
