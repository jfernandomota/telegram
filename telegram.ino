#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>

#define PIN_MOV_SENSOR D1
#define PIN_FUM_SENSOR_A A0
#define PIN_FUM_SENSOR D2
#define PIN_TEMP_SENSOR D3
#define PIN_LIGHT_RELE D6
#define PIN_FAN_RELE D7

WiFiClient client;
PubSubClient MQTT(client);

const char *SSID = "JFernando";
const char *WIFI_PASSWORD = "03099660";
const char *BROKER_MQTT = "192.168.43.56";
int BROKER_PORT = 1883;

DHT dht(PIN_TEMP_SENSOR, DHT11);

boolean isLightOn = false;
boolean isFanOn = false;
boolean isMovOn = true;

void setup()
{
    Serial.begin(115200);

    pinMode(PIN_MOV_SENSOR, INPUT);
    pinMode(PIN_FUM_SENSOR, INPUT);
    pinMode(PIN_FUM_SENSOR_A, INPUT);
    pinMode(PIN_LIGHT_RELE, OUTPUT);
    pinMode(PIN_FAN_RELE, OUTPUT);
    initWifi();
    initMQTT();
}

void loop()
{
    if (WiFi.status() != WL_CONNECTED)
    {
        initWifi();
    }

    if (!MQTT.connected())
    {
        initMQTT();
    }

    float humidade = dht.readHumidity();
    float temperatura = dht.readTemperature();

    int movimento = digitalRead(PIN_MOV_SENSOR);
    int fumaca = digitalRead(PIN_FUM_SENSOR);
    float nivelFumaca = analogRead(PIN_FUM_SENSOR_A);

    digitalWrite(PIN_LIGHT_RELE, isLightOn ? HIGH : LOW);
    digitalWrite(PIN_FAN_RELE, isFanOn ? HIGH : LOW);

    boolean hasPresence = (movimento == HIGH) && isMovOn;

    MQTT.publish("telegram/movement", String(hasPresence).c_str());
    MQTT.publish("telegram/smoke", String(fumaca == LOW).c_str());
    MQTT.publish("telegram/smokea", String(nivelFumaca).c_str());
    MQTT.publish("telegram/humidity", String(humidade).c_str());
    MQTT.publish("telegram/temperature", String(temperatura).c_str());
    MQTT.publish("telegram/light", String(isLightOn).c_str());
    MQTT.publish("telegram/fan", String(isFanOn).c_str());
    MQTT.publish("telegram/movsensor", String(isMovOn).c_str());

    MQTT.loop();

    delay(1000);
}

void initWifi()
{
    WiFi.begin(SSID, WIFI_PASSWORD);

    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.println("Couldn't get a wifi connection");
        delay(2000);
    }

    Serial.println("Connected to wifi");
}

void initMQTT()
{
    MQTT.setServer(BROKER_MQTT, BROKER_PORT);
    MQTT.setCallback(mqtt_message);

    while (!MQTT.connected())
    {
        Serial.print("* Tentando se conectar ao Broker MQTT: ");

        if (MQTT.connect(WiFi.macAddress().c_str()))
        {
            Serial.println("Broker connected");
            MQTT.subscribe("telegram/action");
        }

        delay(2000);
    }
}

void mqtt_message(char *topic, byte *payload, unsigned int length)
{
    char operation = (char) payload[0];
    
    Serial.println(operation);

    switch (operation) {
        case '1':
            isLightOn = true;
            break;
        case '2':
            isLightOn = false;
            break;
        case '3':
            isFanOn = true;
            break;
        case '4':
            isFanOn = false;
            break;
        case '5':
            isFanOn = true;
            isLightOn = true;
            break;
        case '6':
            isFanOn = false;
            isLightOn = false;
            break;
        case '7':
            isMovOn = false;
            break;
        case '8':
            isMovOn = true;
    }
}